package net.phippy.rest.openapi.OpenApiMonuments;

import lombok.AllArgsConstructor;
import net.phippy.rest.monuments.MonumentsRepository;
import net.phippy.rest.monuments.model.Monuments;
import net.phippy.rest.monuments.model.MonumentsResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor
public class OpenApiMonumentsService implements OpenApiMonumentsImpl {
    private final MonumentsRepository monumentsRepository;

    public ResponseEntity<?> allMonuments() {
        List<Monuments> monuments = monumentsRepository.findAll();
        List<MonumentsResponse> monumentsResponses = new ArrayList<>();

        for (Monuments monument: monuments) {
            MonumentsResponse monumentsResponse = MonumentsResponse.createMonumentsResponse(monument);
            monumentsResponse.setLinkToPhoto("https://api.phippy.net/open-api/monuments/random-monuments-photo?id=" + monument.getId());
            monumentsResponses.add(monumentsResponse);
        }

        return new ResponseEntity<>(monumentsResponses, HttpStatus.OK);
    }

    public ResponseEntity<?> randomMonuments() {
        Monuments monuments = monumentsRepository.randomMonuments().get(0);
        MonumentsResponse monumentsResponse = MonumentsResponse.createMonumentsResponse(monuments);
        monumentsResponse.setLinkToPhoto("https://api.phippy.net/open-api/monuments/random-monuments-photo?id=" + monuments.getId());

        return new ResponseEntity<>(monumentsResponse, HttpStatus.OK);
    }

    public ResponseEntity<byte[]> getImagePhoto(long id) {
        return new ResponseEntity<>(monumentsRepository.getReferenceById(id).getPhoto(), HttpStatus.OK);
    }
}
