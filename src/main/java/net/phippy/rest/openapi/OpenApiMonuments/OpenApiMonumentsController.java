package net.phippy.rest.openapi.OpenApiMonuments;

import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping(path = "/open-api/monuments")
public class OpenApiMonumentsController {
    private final OpenApiMonumentsService openApiMonumentsService;

    @GetMapping({"/all-monuments"})
    public ResponseEntity<?> allMonuments() {
        return openApiMonumentsService.allMonuments();
    }

    @GetMapping({"/random-monuments"})
    public ResponseEntity<?> randomMonuments() {
        return openApiMonumentsService.randomMonuments();
    }

    @GetMapping(value = "/random-monuments-photo", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody ResponseEntity<byte[]> getImagePhoto(@RequestParam long id) {
        return openApiMonumentsService.getImagePhoto(id);
    }
}
