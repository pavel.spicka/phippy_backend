package net.phippy.rest.openapi.OpenApiMonuments;

import org.springframework.http.ResponseEntity;

public interface OpenApiMonumentsImpl {
    ResponseEntity<?> allMonuments();
    ResponseEntity<?> randomMonuments();
    ResponseEntity<byte[]> getImagePhoto(long id);
}
