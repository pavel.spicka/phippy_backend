package net.phippy.rest.secure.OpenApiMonuments;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping(path = "/secure/monuments")
@SecurityRequirement(name = "Secure")
public class OpenApiMonumentsSecureController {
    private final OpenApiMonumentsSecureService openApiMonumentsSecureService;

    @GetMapping({"/all-monuments"})
    public ResponseEntity<?> allMonuments() {
        return openApiMonumentsSecureService.allMonuments();
    }

    @GetMapping({"/random-monuments"})
    public ResponseEntity<?> randomMonuments() {
        return openApiMonumentsSecureService.randomMonuments();
    }

    @GetMapping(value = "/random-monuments-photo", produces = MediaType.IMAGE_JPEG_VALUE)
    public @ResponseBody ResponseEntity<byte[]> getImagePhoto(@RequestParam long id) {
        return openApiMonumentsSecureService.getImagePhoto(id);
    }
}
