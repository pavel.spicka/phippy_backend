package net.phippy.rest.secure.OpenApiMonuments;

import org.springframework.http.ResponseEntity;

public interface OpenApiMonumentsSecureImpl {
    ResponseEntity<?> allMonuments();
    ResponseEntity<?> randomMonuments();
    ResponseEntity<byte[]> getImagePhoto(long id);
}
