package net.phippy.rest.monuments.model;

import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Builder
@ToString
public class Monuments {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO) private Long id;
    private String name;
    private String country;
    private String continent;
    private String coordinates;
    private String url;
    @Lob
    @Column(length = 65555)
    private String description;
    @Lob
    @Column(length = 65555)
    private byte[] photo;
}
