package net.phippy.rest.monuments.model;

import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class MonumentsResponse {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO) private Long id;
    private String name;
    private String country;
    private String continent;
    private String coordinates;
    private String url;
    private String description;
    private String linkToPhoto;

    public static MonumentsResponse createMonumentsResponse(Monuments monuments) {
        return new MonumentsResponse(monuments.getId(),
                monuments.getName(),
                monuments.getCountry(),
                monuments.getContinent(),
                monuments.getCoordinates(),
                monuments.getUrl(),
                monuments.getDescription(),
                "");
    }
}
