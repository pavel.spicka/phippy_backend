package net.phippy.rest.monuments;

import io.swagger.v3.oas.annotations.Hidden;
import net.phippy.rest.monuments.model.Monuments;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Hidden
@Repository
public interface MonumentsRepository extends JpaRepository<Monuments, Long> {

    @Query(value = "SELECT * FROM monuments m ORDER BY RAND() LIMIT 1", nativeQuery = true)
    List<Monuments> randomMonuments();

    @Query(value = "SELECT * FROM monuments m ORDER BY RAND() LIMIT 3", nativeQuery = true)
    List<Monuments> random3Monuments();
}
