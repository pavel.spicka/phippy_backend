package net.phippy.config;

import com.google.gson.GsonBuilder;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.log4j.Log4j2;
import net.phippy.config.security.service.TokenProvider;
import net.phippy.config.security.service.VerifyResponse;
import net.phippy.config.security.util.SecurityCipher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Log4j2
@Service
public class TokenAuthenticationFilter extends OncePerRequestFilter {
    public static final String accessTokenCookieName = "accessToken";

    @Value("${phippy.iam.verify.endpoint}")
    private String phippyIamVerifyEndpoint;

    @Value("${master.iam.password}")
    private String masterIAMPassword;
    @Autowired
    private TokenProvider tokenProvider;

    public TokenAuthenticationFilter() {
    }

    UserDetails getUserDetails(String jwtToken) {
        try {
            HttpResponse<String> response = HttpClient.newHttpClient().send(HttpRequest
                    .newBuilder()
                    .uri(URI.create(phippyIamVerifyEndpoint))
                    .POST(HttpRequest.BodyPublishers.ofString("{\"jwtToken\":\"" + jwtToken + "\", \"phippyIAMPassword\" : \"" + masterIAMPassword + "\"}"))
                    .headers("Accept", "application/hal+json", "Content-Type", "application/json")
                    .build(), HttpResponse.BodyHandlers.ofString());
            VerifyResponse verifyResponse = new GsonBuilder().create().fromJson(response.body(), VerifyResponse.class);
            return verifyResponse.getUserDetails();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
            throw new UsernameNotFoundException("Problem with phippy iam verify endpoint or master iam password.");
        }
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        try {
            String jwt = getJwtToken(httpServletRequest, true);
            if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt)) {
                UserDetails userDetails = getUserDetails(jwt);
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } catch (Exception ex) { }

        final String authorizationHeader = httpServletRequest.getHeader("Authorization");

        String email = null;
        String jwt = null;

        if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
            jwt = authorizationHeader.substring(7);
            email = tokenProvider.getEmailFromToken(jwt);
        }


        if (email != null && SecurityContextHolder.getContext().getAuthentication() == null) {

            UserDetails userDetails = getUserDetails(jwt);

            if (tokenProvider.validateToken(jwt)) {

                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                        new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());

                usernamePasswordAuthenticationToken.setDetails(
                        new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));

                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }


        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

    private String getJwtFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader("Authorization");
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            String accessToken = bearerToken.substring(7);
            if (accessToken == null) return null;

            return SecurityCipher.decrypt(accessToken);
        }
        return null;
    }

    private String getJwtFromCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        for (Cookie cookie : cookies) {
            if (accessTokenCookieName.equals(cookie.getName())) {
                String accessToken = cookie.getValue();
                if (accessToken == null) return null;

                return SecurityCipher.decrypt(accessToken);
            }
        }
        return null;
    }

    private String getJwtToken(HttpServletRequest request, boolean fromCookie) {
        if (fromCookie) return getJwtFromCookie(request);

        return getJwtFromRequest(request);
    }
}